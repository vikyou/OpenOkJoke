package com.xiaolei.openokjoke.Beans

class JokeBean(var id: Int,
               var page: String,
               var type: String,
               var title: String)
{
    var content: String? = null
        get() = field?.replace("<br />","")?.replace("<br>","")
    enum class Type
    {
        txt, jpg, gif
    }
}